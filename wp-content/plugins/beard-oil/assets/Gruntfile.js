module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    postcss: {
      options: {
        map: true, // inline sourcemaps 

        // or 
        map: {
            inline: false, // save all sourcemaps as separate files... 
            annotation: '/' // ...to the specified directory 
        },
        
        processors: [
	    require('autoprefixer')({ grid: true }), // add vendor prefixes and IE grid support, see package.json for Browserslist config          
	    require('cssnano')(), // minify the result 
            require('postcss-flexibility'), // add vendor prefix for flex-fallback
        ]
      },
      dist: {
        src: '*.css'
      }
    },
    watch: {
      css: {
        files: ['scss/*.scss', '**/**/*.scss'],
        // tasks: ['prettier','sass'],
	tasks: ['sass'],
      },
      // scripts: {
      //  files: ['**/*.css'],
      //  tasks: ['postcss'],
      // },
    },
    sass: {
    dist: {
      files: {
        'style.css': 'scss/style.scss'
      }
    }
  },
  prettier: {
    options: {
      singleQuote: true,
      useTabs: true,
      progress: true // By default, a progress bar is not shown. You can opt into this behavior by passing true.
    },
    files: {
      src: [
        "scss/elements/*.scss",
        "scss/pages/*.scss",
        "!scss/elements/_rows-columns.scss"
      ],
  },
  },
  });

  grunt.loadNpmTasks('@lodder/grunt-postcss');  
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-prettier');
  grunt.registerTask('default', ['postcss']);

};
